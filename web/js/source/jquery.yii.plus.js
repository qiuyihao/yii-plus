/**
 * Base Javascript Library
 * Author: Horace Yau (qiuyihao@139.com)
 */

var YiiPlus = {};
YiiPlus.UI = {};
YiiPlus.history = {};

YiiPlus.isString = function(v){
	return ((typeof(v)=="string") ? true : false);
}
YiiPlus.isUndefined = function(v){
	return ((typeof(v)=="undefined") ? true : false);
}
YiiPlus.goto = function(url, timer){
	setTimeout(function(){
		url ? (window.location.href = url) : (window.location.reload());
	}, timer);
}
YiiPlus.history.back = function(){
	self.location = document.referrer;
}

YiiPlus.$_GET = (function(){
	var url = window.document.location.href.toString();
	var u = url.split("?");
	if(typeof(u[1]) == "string"){
		u = u[1].split("&");
		var get = {};
		for(var i in u){
			var j = u[i].split("=");
			get[j[0]] = j[1];
		}
		return get;
	} else {
		return {};
	}
})();

YiiPlus.isFormChanged = function(formID){
	var isChanged = false;
	// text|hidden|file|textarea => objControl.value != objControl.defaultValue
	// checkbox => objControl.checked != objControl.defaultChecked
	if(!isChanged){
		var elems = document.getElementById(formID).getElementsByTagName("input");
		for (var i=0; i<elems.length; i++) {
			if('text|number|hidden'.indexOf(elems[i].type) >= 0){
				if (elems[i].value != elems[i].defaultValue) {
					isChanged = true;
				}
			}
			else if('radio|checkbox'.indexOf(elems[i].type) >= 0){
				if (elems[i].checked != elems[i].defaultChecked) {
					isChanged = true;
				}
			}
		}
	}
	// select-one => objControl.defaultSelected
	return isChanged;
}

/**
 * Response: {"ret"=>"1", "msg"=>"{"username":"username can't be blank", "password":"password is short"}"}
 * @param formID
 * @param url
 */
YiiPlus.yiiFormSubmit = function(options){
	var defaults = {
		formID: '',
		modelID: '',
		url: '',
		dataType: 'post',
		errorRender: 'field',
		success: null
	};
	options = $.extend(defaults, options);
	var formObj = YiiPlus.isString(options.formID) ? $('#'+options.formID) : options.formID;
	formObj.find('.errorSummary').html('').hide();
	formObj.find('input').removeClass('form-control-error');
	formObj.find('select').removeClass('form-control-error');
	formObj.find('.errorMessage').html('').show();
	// Ajax request
	formObj.ajaxSubmit({
		url: options.url,
		type: options.dataType,
		success: function(data, status){
			data = JSON.parse(data);
			// No error
			if(data.ret=='0'){
				formObj.find('input').removeClass('form-control-error');
				formObj.find('.errorMessage').hide();
				options.success(data);
			}
			// Has error
			else if(data.ret=='1'){
				if(YiiPlus.isString(data.msg)){
					YiiPlus.UI.alertTop(data.msg, 'error');
					return;
				}
				// Field render
				if(options.errorRender.indexOf('field') >= 0){
					for(field in data.msg) {
						var inputID = options.modelID+'_'+field;
						formObj.find('#'+inputID).addClass('form-control-error');
						formObj.find('#'+inputID+'_em_').addClass('errorMessage').html(data.msg[field]).show();
					}
				}
				// Summary render
				else if(options.errorRender.indexOf('summary') >= 0){
					var summary = '';
					for(field in data.msg) {
						summary += data.msg[field] + '<br/>';
					}
					formObj.find('.errorSummary').html(summary).show();
				}
			}
		}
	});
}


YiiPlus.yiiFormParamErrorsToString = function(msg){
	var summary = '';
	for(i in msg) {
		summary += msg[i] + "\n";
	}
	return (summary);
}

/**
 * YiiPlus UI alertTop, alert a dialog
 * @param text
 * @param type (null,error)
 */
YiiPlus.UI.alertTopTimer = null;
YiiPlus.UI.alertTop = function(text, type){
	var cssClass = 'YiiPlus_alertTopType_' + ((YiiPlus.isUndefined(type)) ? '' : type);
	var html = '<div class="YiiPlus_alertTop"><div class="YiiPlus_alertTopText ' + cssClass + '">'+ text + '</div></div>';
	clearTimeout(YiiPlus.UI.alertTopTimer);
	$('.YiiPlus_alertTop').remove();
	$('body').append(html);
	YiiPlus.UI.alertTopTimer = setTimeout(function(){
		$('.YiiPlus_alertTop').remove();
	}, 5000);
}

/**
 * YiiPlus UI renderTemplate
 * @param tplID
 * @param data
 * @returns {*}
 */
Handlebars.registerHelper("equal",function(v1,v2,options){
	if(v1==v2){
		return options.fn(this);
	}else{
		return options.inverse(this);
	}
});
Handlebars.registerHelper("range", function(v1,v2,v3,options){
	if(v2>=v1 && v2<=v3){
		return options.fn(this);
	}else{
		return options.inverse(this);
	}
});
Handlebars.registerHelper("indexOf", function(v1,v2,options){
	if(!YiiPlus.isUndefined(v1) && v1!='' && v1!=null && v1.indexOf(v2)>=0){
		return options.fn(this);
	}else{
		return options.inverse(this);
	}
});
YiiPlus.UI.renderTemplate = function(tplID, data) {
	var template = Handlebars.compile($('#'+tplID).html());
	if(YiiPlus.isString(data)){
		data = JSON.parse((data=='')?null:data);
	}
	return (template(data));
}


YiiPlus.UI.modal = function(opts) {
	var that = this;
	that.ui = null;
	var defaults = {
		title: '提示',
		render: 'text',
		msg: '提示文本',
		url: '',
		hideBtnOK: false,
		textBtnOK: '保存',
		textBtnCancel: '取消',
		ready: null,
		success: null
	};
	opts = $.extend(defaults, opts);
	var modalHTML = '<div class="modal fade" tabindex="-1" role="dialog">' +
		'<div class="modal-dialog" role="document">' +
			'<div class="modal-content">' +
				'<div class="modal-header">' +
					'<button type="button" class="modal-btn-close close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
					'<h4 class="modal-title">' + opts.title + '</h4>' +
				'</div>' +
				'<div class="modal-body" style="background:#fff;">' +
				'</div>' +
				'<div class="modal-footer">' +
					'<button type="button" class="modal-btn-close btn btn-default" data-dismiss="modal">'+opts.textBtnCancel+'</button>' +
					(opts.hideBtnOK ? '' : '<button type="button" class="modal-btn-ok btn btn-primary" style="margin-left:20px;">'+opts.textBtnOK+'</button>') +
				'</div>' +
			'</div>' +
		'</div>' +
	'</div>';

	var modalObj = $(modalHTML).modal();
	that.ui = modalObj;
	if(opts.ready) {
		opts.ready();
	}

	that.close = function() {
		that.ui.remove();
		if($('.modal').length == 0) {
			$('body').removeClass('modal-open');
			$('.modal-backdrop').remove();
		}
	}
	modalObj.find('.modal-btn-ok').click(function(){
		if(opts.success() == true) {
			that.close();
		}
	});
	modalObj.on('hide.bs.modal', function(){
		that.close();
	});
	if(opts.render == 'ajax') {
		$.get(opts.url, function(data){
			modalObj.find('.modal-body').html(data);
		});
	} else {
		modalObj.find('.modal-body').html(opts.msg);
	}
	return that;
}

/**
 * YiiPlus UI selectSetDefaultOption
 * @param selector
 */
YiiPlus.UI.selectSetDefaultOption = function(selector) {
	selector.each(function(){
		var select = $(this);
		var value = $(this).attr('value');
		if(value!=''){
			select.find('option').each(function(){
				if($(this).attr('value')==value){
					$(this).attr('selected', true);
				}
			});
		}
	});
}

jQuery.fn.YiiPlus_selectSetValue = function(value){
	$(this).find('option').each(function(){
		if($(this).attr('value') == value) {
			$(this).attr('selected', true);
			return true;
		}
	});
}

/**
 * YiiPlus UI pager
 * @param options
 */
jQuery.fn.YiiPlus_Pager = function(opts) {
	var that = this;
	var defaults = {
		method: 'GET',
		url: '',
		data: {},
		page: 1,
		pagesize: 10,
		success: null
	};
	opts = $.extend(defaults, opts);

	that.getOpts = function(){
		return opts;
	}

	that.reloadPager = function(){
		renderPager();
		renderDatas();
	}

	that.initPager = function(){
		that.getOpts().page = 1;
		renderPager();
		renderDatas();
	}

	function renderPager() {
		var html = '';
		html += '<a class="btn btn-default first">首页</a>';
		html += '<a class="btn btn-default prev" style="margin:0 10px;">上一页</a>';
		html += ('<span style="margin:0 10px;">第<b class="page_index">1</b>页 / 共<strong class="page_num">1</strong>页</a></span>');
		html += '<a class="btn btn-default next" style="margin:0 10px;">下一页</a>';
		html += '<a class="btn btn-default last" style="">尾页</a>';
		html += '<span class="jump" style="margin-left:20px;">跳至第 <select class="form-control" style="width:50px;display:inline-block;"></select> 页</a></span>';
		that.html('').append(html);

		that.find('.first').click(function(){
			opts.page = 1;
			renderDatas();
		});
		that.find('.prev').click(function(){
			opts.page -= 1;
			renderDatas();
		});
		that.find('.next').click(function(){
			opts.page += 1;
			renderDatas();
		});
		that.find('.last').click(function(){
			opts.page = opts.pages;
			renderDatas();
		});
		that.find('select').change(function(){
			opts.page = $(this).val();
			renderDatas();
		});
	}

	function renderDatas() {
		opts.data.page = opts.page;
		opts.data.pagesize = opts.pagesize;
		$.ajax({
			type: opts.method,
			url: opts.url,
			data: opts.data,
			dataType: 'json',
			success: function(res){
				if(!res.datas || YiiPlus.isUndefined(res.datas.page)){
					opts.page = 1;
					opts.data_total_num = 0;
					opts.pages = 1;
				}
				else{
					opts.page = res.datas.page;
					opts.pagesize = res.datas.pagesize;
					opts.data_total_num = res.datas.data_total_num;
					opts.pages = parseInt((opts.data_total_num-1)/opts.pagesize) + 1;
				}
				if(opts.success) {
					opts.success(res);
				}

				that.find('select').html('');
				that.find('.page_index').html(opts.page);
				that.find('.page_num').html(opts.pages);
				that.find('.first').attr('disabled', ((opts.page==1)?true:false));
				that.find('.prev').attr('disabled', ((opts.page==1)?true:false));
				that.find('.next').attr('disabled', ((opts.page==opts.pages)?true:false));
				that.find('.last').attr('disabled', ((opts.page==opts.pages)?true:false));
				for(var i=1; i<=opts.pages; i++){
					that.find('select').append('<option value="'+i+'">'+i+'</option>');
				}
				that.find('select').YiiPlus_selectSetValue(opts.page);
			}
		});
	}

	return that;
}


jQuery.fn.YiiPlus_CountdownBtn = function(options) {
	var that = this;
	var timer = null;
	var defaults = {
		text: '获取验证码',
		interval: 1000,
		onStartup: null,
		onInterval: null
	};
	opts = $.extend(defaults, options);

	that.startup = function() {
		that.attr('disabled', true);
		if(opts.onStartup()){
			timer = setInterval(function(){
				opts.onInterval();
			}, opts.interval);
		}
		else{
			that.html(opts.text).attr('disabled', false);
		}
	}

	that.cleanup = function() {
		clearInterval(timer);
		that.attr('disabled', false);
	}

	that.recoverBtn = function() {
		that.html(opts.text).attr('disabled',false);
	}

	that.disableBtn = function(disabled){
		return (that.attr('disabled', disabled));
	}

	return that;
}