<?php

/**
 * Class YiiPlus
 */
class YiiPlus
{
	const AUTHOR = '15818841883';

	public static function getGPC($k, $type='GP', $v=NULL) {
		$type = strtoupper($type);
		switch($type) {
			case 'G': $var = &$_GET; break;
			case 'P': $var = &$_POST; break;
			case 'C': $var = &$_COOKIE; break;
			default:
				if(isset($_GET[$k])) {
					$var = &$_GET;
				} else {
					$var = &$_POST;
				}
				break;
		}
		return isset($var[$k]) ? $var[$k] : $v; //注意这里没有设置的时候返回的是NULL.
	}

	public static function randomInteger($length)
	{
		$hash = '';
		$chars = '123456789';
		$max = strlen($chars) - 1;
		mt_srand((double)microtime() * 1000000);
		for($i = 0; $i < $length; $i ++) {
			$hash .= $chars[mt_rand(0, $max)];
		}
		return $hash;
	}

	public static function randomString($length, $numeric = 0)
	{
		$seed = base_convert(md5(microtime().$_SERVER['DOCUMENT_ROOT']), 16, $numeric ? 10 : 35);
		$seed = $numeric ? (str_replace('0', '', $seed).'012340567890') : ($seed.'zZ'.strtoupper($seed));
		$hash = '';
		$max = strlen($seed) - 1;
		for($i = 0; $i < $length; $i++) {
			$hash .= $seed{mt_rand(0, $max)};
		}
		return $hash;
	}

	public static function dmkdir($dir, $mode = 0777, $makeindex = TRUE)
	{
		if(!is_dir($dir)) {
			self::dmkdir(dirname($dir), $mode, $makeindex);
			@mkdir($dir, $mode);
			if(!empty($makeindex)) {
				@touch($dir.'/index.html'); @chmod($dir.'/index.html', 0777);
			}
		}
		return true;
	}

	public static function getRemoteIP()
	{
		if(!empty($_SERVER["HTTP_CLIENT_IP"]))
			$cip = $_SERVER["HTTP_CLIENT_IP"];
		else if(!empty($_SERVER["HTTP_X_FORWARDED_FOR"]))
			$cip = $_SERVER["HTTP_X_FORWARDED_FOR"];
		else if(!empty($_SERVER["REMOTE_ADDR"]))
			$cip = $_SERVER["REMOTE_ADDR"];
		else
			$cip = "";
		return $cip;
	}

	/**
	 * @param $models
	 * @param array $filterAttributes
	 * @return array
	 * YII 把yii多层对象转成数组和统一json返回值
	 * http://www.tuicool.com/articles/IJfyIn
	 */
	public static function yiiModelToArray($models, array $filterAttributes = null)
	{
		if(null == $models)
		{
			return array();
		}
		if (is_array($models))
			$arrayMode = TRUE;
		else {
			$models = array($models);
			$arrayMode = FALSE;
		}
		$result = array();
		foreach ($models as $model) {
			$attributes = $model->getAttributes();
			if (isset($filterAttributes) && is_array($filterAttributes)) {
				foreach ($filterAttributes as $key => $value) {
					if (strtolower($key) == strtolower($model->tableName()) && strpos($value, '*') === FALSE) {
						$value = str_replace(' ', '', $value);
						$arrColumn = explode(",", $value);
						foreach ($attributes as $key => $value)
							if (!in_array($key, $arrColumn))
								unset($attributes[$key]);
					}
				}
			}
			$relations = array();
			foreach ($model->relations() as $key => $related) {
				if ($model->hasRelated($key)) {
					if(($model->$key instanceof CModel) || is_array($model->$key)){
						$relations[$key] = self::yiiModelToArray($model->$key, $filterAttributes);
					} else {
						$relations[$key] = $model->$key;
					}
				}
			}
			$all = array_merge($attributes, $relations);
			if ($arrayMode)
				array_push($result, $all);
			else
				$result = $all;
		}
		return $result;
	}

	public static function yiiFormResponse($ret, $msg, $datas = '')
	{
		if(intval($ret)==1) {
			echo CJSON::encode(array('ret'=>intval($ret), 'msg'=>$msg));
			return true;
		}
		if(null!=$datas && is_object($datas)) {
			$datas = YiiPlus::yiiModelToArray($datas);
		}
		echo CJSON::encode(array('ret'=>intval($ret), 'msg'=>$msg, 'datas'=>$datas));
		return true;
	}

	public static function yiiModelPagination($criteria, $model, $wantPage = 1, $pageSize = 10)
	{
		if(null == $wantPage){ $wantPage = YiiPlus::getGPC('page'); }
		if(null == $pageSize){ $pageSize = YiiPlus::getGPC('pagesize'); }
		if(null == $criteria) {
			$criteria = new CDbCriteria();
		}
		$count = $model->count($criteria);
		$page = new CPagination($count);
		$page->setCurrentPage($wantPage - 1);
		$page->setPageSize($pageSize);
		$page->applyLimit($criteria);
		$content = $model->findAll($criteria);

		return ($content==null) ? null : array(
			'pager' => $page,
			'content' => $content,
		);
	}

	public static function yiiFormResponsePage($ret, $msg, $pagination, $start = 1)
	{
		if($pagination == null) {
			return self::yiiFormResponse($ret, $msg, array());
		}
		$pager = $pagination['pager'];
		return self::yiiFormResponse($ret, $msg, array(
			'page' => $pager->currentPage + $start,
			'pagesize' => $pager->pageSize,
			'data_total_num' => $pager->itemCount,
			'content' => $pagination['content'],
		));
	}

	public static function phpExcel($callback)
	{
		//  close Yii autoload
		spl_autoload_unregister(array('YiiBase', 'autoload'));
		$phpExcelPath = Yii::getPathOfAlias('system.vendors.PHPExcel');

		include_once($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
		$ret = $callback();

		// recover Yii autoload
		spl_autoload_register(array('YiiBase', 'autoload'));

		return $ret;
	}
}