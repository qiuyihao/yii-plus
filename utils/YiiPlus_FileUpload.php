<?php

class YiiPlus_FileUpload
{
	public $uploadName = 'file';
	public $extensionWhitelist = null;
	public $maxFileSize = 100000;
	public $savePath = '';
	public $saveFileName = '';

    private $error = '';
	private $_fileName = '';
	public $_fileExtension = '';

	/**
	 * @return bool|string
	 */
	public function upload()
	{
		// Code for Session Cookie workaround
		//if (isset($_POST["PHPSESSID"])) {
		//	session_id($_POST["PHPSESSID"]);
		//} else if (isset($_GET["PHPSESSID"])) {
		//	session_id($_GET["PHPSESSID"]);
		//}
		//session_start();

		$POST_MAX_SIZE = ini_get('post_max_size');
		$unit = strtoupper(substr($POST_MAX_SIZE, -1));
		$multiplier = ($unit == 'M' ? 1048576 : ($unit == 'K' ? 1024 : ($unit == 'G' ? 1073741824 : 1)));

		if ((int)$_SERVER['CONTENT_LENGTH'] > $multiplier*(int)$POST_MAX_SIZE && $POST_MAX_SIZE) {
			header("HTTP/1.1 500 Internal Server Error");
			echo "POST exceeded maximum allowed size.";
			return false;
		}

		if(!$this->validate()) {
			return false;
		}

		// 检查文件名
		// $MAX_FILENAME_LENGTH = 260;
		// $valid_chars_regex = '.A-Z0-9_ !@#$%^&()+={}\[\]\',~`-'; //允许的文件名字符
		//$file_name = preg_replace('/[^'.$valid_chars_regex.']|\.+$/i', "", basename($_FILES[$upload_name]['name']));
		//if (strlen($file_name) == 0 || strlen($file_name) > $MAX_FILENAME_LENGTH) {
		//	$this->HandleError("Invalid file name");
		//	return false;
		//}
		//if (file_exists($save_path . $file_name)) {
		//	$this->HandleError("File with this name already exists");
		//	return false;
		//}

		// save file
		$upload_name = $this->uploadName;
		$save_path_local = getcwd().$this->savePath;
		YiiPlus::dmkdir($save_path_local);
		if (!@move_uploaded_file($_FILES[$upload_name]["tmp_name"], $save_path_local.$this->_fileName)) {
			$this->HandleError("文件无法保存.");
			return false;
		}

		return ($this->savePath.$this->_fileName);
	}

	public function validate()
	{
		// validate FILES
		if(!$this->validateFILES()){
			return false;
		}
		// validate file size
		if(!$this->validateFileSize()) {
			return false;
		}
		// validate file extension
		if(!$this->validateFileExtension()) {
			return false;
		}
		// validate file name
		if(!$this->validateFileName()) {
			return false;
		}
		$save_path_local = getcwd().$this->savePath;
		YiiPlus::dmkdir($save_path_local);
		return true;
	}

	/**
	 * @return bool
	 */
	public function validateFILES()
	{
		$upload_name = $this->uploadName;
		$uploadErrors = array(
			0=>"文件上传成功",
			1=>"上传的文件超过了 php.ini 文件中的 upload_max_filesize directive 里的设置",
			2=>"上传的文件超过了 HTML form 文件中的 MAX_FILE_SIZE directive 里的设置",
			3=>"上传的文件仅为部分文件",
			4=>"没有文件上传",
			6=>"缺少临时文件夹"
		);

		if (!isset($_FILES[$upload_name])) {
			$this->HandleError("No upload found in \$_FILES for " . $upload_name);
			return false;
		} else if (isset($_FILES[$upload_name]["error"]) && $_FILES[$upload_name]["error"] != 0) {
			$this->HandleError($uploadErrors[$_FILES[$upload_name]["error"]]);
			return false;
		} else if (!isset($_FILES[$upload_name]["tmp_name"]) || !@is_uploaded_file($_FILES[$upload_name]["tmp_name"])) {
			$this->HandleError("Upload failed is_uploaded_file test.");
			return false;
		} else if (!isset($_FILES[$upload_name]['name'])) {
			$this->HandleError("File has no name.");
			return false;
		}
		return true;
	}

	/**
	 * @return bool
	 */
	public function validateFileSize()
	{
		$file_size = @filesize($_FILES[$this->uploadName]["tmp_name"]);
		if (!$file_size || $file_size > $this->maxFileSize) {
			$this->HandleError("文件不能超过".intval($this->maxFileSize/1000).'KB');
			return false;
		}
		if ($file_size <= 0) {
			$this->HandleError("文件大小无效，文件被损坏");
			return false;
		}
		return true;
	}

	/**
	 * @return bool
	 */
	public function validateFileExtension()
	{
		if($this->extensionWhitelist && !in_array($_FILES[$this->uploadName]["type"], $this->extensionWhitelist)) {
			$this->HandleError("上传的文件格式不对");
			return false;
		}

		return true;

//		$path_info = pathinfo($_FILES[$this->uploadName]['name']);
//		$this->_fileExtension = $path_info["extension"];
//		$is_valid_extension = false;
//		foreach ($this->extensionWhitelist as $extension) {
//			if (strcasecmp($this->_fileExtension, $extension) == 0) {
//				$is_valid_extension = true;
//				break;
//			}
//		}
//		if (!$is_valid_extension) {
//			$this->HandleError("上传的文件格式不对");
//			return false;
//		}
//		return true;
	}

	public function validateFileName()
	{
		$path_info = pathinfo($_FILES[$this->uploadName]['name']);
		$this->_fileExtension = $path_info["extension"];
		$this->_fileName = (time() . YiiPlus::randomInteger(6) . '.' . $this->_fileExtension);
		$this->saveFileName = $this->_fileName;
		return true;
	}

	private function HandleError($message) {
		$this->error = $message;
	}

	public function getError() {
		return $this->error;
	}

	/**
	 * @return mixed
	 */
	public function getTmpNameFromFILES()
	{
		return (isset($_FILES[$this->uploadName])) ? $_FILES[$this->uploadName]["tmp_name"] : '';
	}
}