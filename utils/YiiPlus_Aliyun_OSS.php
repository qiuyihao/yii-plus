<?php

defined('ALIOSS_SIZE_MAX') or define('ALIOSS_SIZE_MAX', 300000000); //300M
defined('ALIOSS_SIZE_MIN') or define('ALIOSS_SIZE_MIN', 0);

include_once 'aliyun_oss/sdk.class.php';

/**
 * Class YiiPlus_Aliyun_OSS
 */
class YiiPlus_Aliyun_OSS
{
	private $oss_access_id;
	private $oss_access_key;
	private $oss_domain;
	private $_extensionWhiteList = null;
	private $_uploadName;

	public $extension;
	public $url;

	public function __construct($access_id = '', $access_key = '', $oss_domain, $uploadName = 'file')
	{
		$this->oss_access_id = $access_id;
		$this->oss_access_key = $access_key;
		$this->oss_domain = $oss_domain;
		$this->_uploadName = $uploadName;
	}

	public function setExtensionWhiteList($extensions)
	{
		$this->_extensionWhiteList = $extensions;
	}

	/**
	 * @param string $path 保存路径(路径末尾不加‘/’)
	 * @param int $minSize 文件大小的最小值，单位：字节
	 * @param int $maxSize 文件大小的最大值，单位：字节
	 * @return array [ret：返回码  msg：返回信息  data：附带内容]
	 *
	 * 返回码：  0：上传成功 data[uploadUrl：访问图片的url]
	 *          1：文件错误 data：null
	 *          2：文件类型和大小不符合 data：null
	 *          3：上传失败 data[uploadRes：object类型的返回数据]
	 */
	public function uploadFile($bucket, $path = '', $minSize = ALIOSS_SIZE_MIN, $maxSize = ALIOSS_SIZE_MAX)
	{
		$uploadName = $this->_uploadName;
		//$arrImgType = ['image/gif', 'image/jpeg', 'image/pjpeg', 'image/jpg', 'image/png', 'image/gif'];

		if($maxSize<=$minSize || $maxSize<0 || $minSize<0) {
			$maxSize = ALIOSS_SIZE_MAX;
			$minSize = ALIOSS_SIZE_MIN;
		}

		//
		if(!isset($_FILES[$uploadName])) {
			return $this->retStruct(2, '没有文件上传(201)');
		}

		//
		$uploadFile = $_FILES[$uploadName];
		if(!isset($_FILES[$uploadName]["tmp_name"]) || !@is_uploaded_file($_FILES[$uploadName]["tmp_name"])) {
			return $this->retStruct(2, '没有文件上传(202)');
		}
		if($this->_extensionWhiteList && !in_array($uploadFile["type"], $this->_extensionWhiteList)) {
			return $this->retStruct(2, '文件类型不符(203)');
		}
		if($uploadFile["size"]<=$minSize || $uploadFile["size"]>=$maxSize) {
			return $this->retStruct(2, '文件大小不符(204)');
		}
		if($uploadFile["error"] > 0) {
			return $this->retStruct(1, $uploadFile["error"]);
		}

		$oss = new ALIOSS($this->oss_access_id, $this->oss_access_key);
		$object = $path . '/' . $this->generateFileName();
		$uploadRes = $oss->upload_file_by_file($bucket, $object, $uploadFile["tmp_name"]);

		if($uploadRes->status == 200) {
			$this->url = $this->oss_domain.'/'.$object;
			return $this->retStruct(0, '上传成功', [
				'uploadUrl' => $this->url,
			]);
		}
		else {
			return $this->retStruct(3, '上传失败:[status]=' . $uploadRes->status, ['uploadRes'=>$uploadRes]);
		}
	}

	public function uploadImgFromLocal($domain, $bucket, $path = '', $localFilePath)
	{
		$oss = new ALIOSS($this->oss_access_id, $this->oss_access_key);
		$object = $path . '/' . $this->generateImgFileName($localFilePath);
		$uploadRes = $oss->upload_file_by_file($bucket, $object, $localFilePath);
		if($uploadRes->status == 200)//成功返回
		{
			return $this->retStruct(0, '上传成功', [
				'uploadUrl' => $domain.$object,
			]);
		}
		else
		{
			return $this->retStruct(3, '上传失败:[status]=' . $uploadRes->status, ['uploadRes'=>$uploadRes]);
		}
	}

	/**
	 * @return string 文件名格式：8位随机字符+时间戳_图片长度_图片高度.上传文件名
	 */
	protected function genImgFileName()
	{
		$uploadFile = $_FILES[$this->_uploadName];
		$imgSize = getimagesize($uploadFile['tmp_name']);
		$name = YiiPlus::randomString(8);
		$qian=array(" ","　","\t","\n","\r");$hou=array("","","","","");
		$name .= time() . "_$imgSize[0]_$imgSize[1]_" . str_replace($qian,$hou,$uploadFile['tmp_name']);
		return $name;
	}

	protected function generateImgFileName($filePath)
	{
		$imgSize = getimagesize($filePath);
		$path_info = pathinfo($filePath);
		$name = BaseFunction::randomString(8) . time() . '_' . $imgSize[0] . '_' . $imgSize[1]. '.' . $path_info["extension"];
		return $name;
	}

	protected function generateFileName()
	{
		$path_info = pathinfo($_FILES[$this->_uploadName]['name']);
		$this->extension = strtolower($path_info["extension"]);
		$filename = (time() . YiiPlus::randomInteger(6) . '.' . $this->extension);
		return $filename;
	}

	/**
	 * @param $ret int 返回码
	 * @param $strMsg string 返回消息
	 * @param null $arrData array 附加信息
	 * @return array 以上字段组成的数组
	 */
	protected function retStruct($ret, $strMsg, $arrData = null)
	{
		$retStruct = [
			'ret' => $ret,
			'msg' => $strMsg,
			'data' => $arrData
		];
		return $retStruct;
	}

	/**
	 * 客户端用来获取OSS上传所需参数，上传成功后客户端返回object名字（包括了路径的），之后加上域名进行本地数据库保存，并返回成功保存数据给客户端，流程结束
	 *
	 * @param int $timeout 参数超时时间（单位：秒）
	 * @return array 返回参数数组：access_id、policy、signature、url
	 */
	public static function getUploadOption($timeout = 300)
	{
		if($timeout<0 || $timeout>1800)//0~30分钟
		{
			$timeout=300;
		}
		$access_id = 'DRzoT6a28PrIYbCQ';
		$access_key = 'Eoxb50bFmOtvTnC5BbXZdnCVADkFTE';
		$bucket = 'xswy';
		$url='http://xswy.oss-cn-shenzhen.aliyuncs.com';
		$time = time()+$timeout;
		$time = date('Y-m-d', $time).'T'.date('H:i:s', $time).'.000Z';
		$policy = '{"expiration": "'.$time.'","conditions":[{"bucket": "'.$bucket.'" },["content-length-range", '.ALIOSS_SIZE_MIN.', '.ALIOSS_SIZE_MAX.']]}';
		$policy = base64_encode($policy);
		$signature = base64_encode(hash_hmac('sha1', $policy, $access_key, true));//生成认证签名
		return ['access_id'=>$access_id, 'policy'=>$policy, 'signature'=>$signature, 'url'=>$url];
	}
}